<?php namespace Config;

class Conexion 
{
	private static $enlace;
	private static $db;
	function __construct()
	{
		self::$db = [
		    'host' => '127.0.0.1',
		    'username' => 'root', //Cambiar al nombre del usuario de BD
		    'password' => '', // Cambiar a la contraseña del usuario de BD
		    'db' => 'tienda' //Cambiar al nombre de tu base de datos
		];
		$this->ConexionBD();
		
	}


	public function ConexionBD(){
		try {
          //print "mysql:host=".self::$db['host'].";dbname=".self::$db['db'];

          $conn = new \PDO("mysql:host=".self::$db['host'].";dbname=".self::$db['db'], self::$db['username'], self::$db['password']);
          self::$enlace = $conn;
          self::$enlace->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
          //$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 		
          //return $this->enlace;
      } catch (\PDOException $exception) {
          exit($exception->getMessage());
      }
	}

	private function getErrorBd(){
		return mysqli_error($this->enlace);
	}

	/**
	 * [consultar description]
	 * @param  [array] $campos      [description]
	 * @param  [string] $tablas      [description]
	 * @param  [string] $condiciones [description]
	 * @param  [array] $groupBy     [description]
	 * @param  [array] $orderBy     [description]
	 * @return [type]              [description]
	 */
	public function consultar($campos = null,$tablas = null, $condiciones = null, $groupBy = null, $orderBy = null){
		$strSql = (!isset($campos)) ? "Select * " : "Select ".implode(",",$campos);
		$strSql.= " From ".$tablas;
		$strSql.= (isset($condiciones)) ? " where 1 = 1 and ".$condiciones : "";
		$strSql.= (isset($groupBy)) ? " group By ".implode(",",$groupBy) : "";
		$strSql.= (isset($orderBy)) ? " order By ".implode(",",$orderBy) : "";

		$this->ejecutarSql($strSql);

	}

	private function ejecutarSql($consulta,$tipoDevolucion = null){		
		try {
		    $resultado = $this->enlace->query($consulta);
		    if($resultado){
		    	while ($fila = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
					$arreglo[] = $fila;
				}
				return $arreglo;
		    }else{
		    	print $consulta."<br>".$this->getErrorBd();	
		    }
		    return $resultado;
		} catch (PDOException $e) {
		    print "¡Error!: " . $e->getMessage() . "<br/>".$consultar;
		    //die();
		}
	}

}


?>