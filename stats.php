<?php 
spl_autoload_register(function($clase){
	$ruta = strtolower(str_replace("\\","/",$clase).".php");
	try
	{
		require_once $ruta;
	}
	catch (Throwable $t)
	{

		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Error Interno del Servidor', true, 500);
		print_r("<h3>Excepcion: </h3> Ha ocurrido un error en el archivo <b>".basename($t->getFile())."</b> en  la linea: {$t->getLine()}: ".$t->getMessage());
	}
	catch (Exception $e)
	{
		print_r("¡Error!: ".$ex->getMessage());
	}
});


try{
	$conexion = new Config\Conexion();
	$servicio = new Service\Producto($conexion);
	
	if($_SERVER["REQUEST_METHOD"]!=="POST"){
		$respuesta = array(
			"codigo"=>"500",
			"mensaje" => "El servicio solo procesara peticiones que viajen por post"
		);

		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Error Interno del Servidor', true, $respuesta["codigo"]);
		echo json_encode($respuesta);
	}else{
		//exit("<pre>".var_export($_POST,true)."</pre>");

		if(isset($_POST["keyword"]) && $_POST["keyword"]){

			$respuesta = array(
				"codigo"=>"200",
				"mensaje" => $servicio::consultar($_POST["keyword"])	
			);
			$mensaje = $_SERVER['SERVER_PROTOCOL'] . ' 200 peticion procesada';
			
		}else{
			$respuesta = array(
				"codigo"=>"500",
				"mensaje" => "No se ha recibido el criterio"
			);
			$mensaje = $_SERVER['SERVER_PROTOCOL'] . ' 500 Error Interno del Servidor';
		}
		

		header($mensaje, true, $respuesta["codigo"]);
		echo json_encode($respuesta);
	}
}catch (Throwable $t)
{

	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Error Interno del Servidor', true, 500);
	print_r("<h3>Excepcion: </h3> Ha ocurrido un error en el archivo <b>".basename($t->getFile())."</b> en  la linea: {$t->getLine()}: ".$t->getMessage());
}catch (Exception $e)
{
   print_r("¡Error!: ".$ex->getMessage());
}


 ?>