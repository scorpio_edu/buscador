Como cargar el csv a la base MySQL

1.- crear la tabla productos con los campos especificados en el csv dentro de la base de datos
2.- seleccionar la tabla y presionar el boton importar
3.- buscar el archivo ".csv" dentro del equipo y seleccionar
4.- hacer clic en l boton continuar dentro la opcion importar datos de phpmyadmin
5.- para visualizar la información cargada, ejecutar la siguiente consulta:
Select * from productos