<!DOCTYPE html>
<html>
<head>
	<title>Buscador</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.32.2/sweetalert2.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Consultar Productos</h3>
				<div class="form-group">
					<label for="search"></label>
					<input type="text" name="search" id="search" class="form-control" placeholder="Indique el nombre del producto">
				</div>
				<div class="form-group">
					<input type="button" id="btn_buscar" class="btn btn-primary" value="consultar">
					<input type="button" id="btn_stats" class="btn btn-success" value="Estadísticas">
				</div>
			</div>
			<div class="col-md-10">
				<table id="tbl-productos" class="table">
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.32.2/sweetalert2.all.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btn_buscar").click(function(){
				buscarProducto($("#search").val().trim());
			});
			$("#btn_stats").click(function(){
				consultarProductos();
			});
		});

		function buscarProducto(valor){
			$.ajax({
				data: {keyword:valor},
				url:"search.php",
				type:"POST",
				success:function(respuesta){
					console.log(respuesta);
				},
				error:function(xhr){
					var respuesta = JSON.parse(xhr.responseText);
					respuesta = eval(respuesta);
					Swal({
					  type: 'error',
					  title: 'Oops...',
					  text: respuesta.mensaje
					})
				}

			});
		}

		function consultarProductos(){
			$.ajax({
				url:"stats.php",
				type:"POST",
				success:function(respuesta){
					console.log(respuesta);
				},
				error:function(xhr){
					var respuesta = JSON.parse(xhr.responseText);
					respuesta = eval(respuesta);
					Swal({
					  type: 'error',
					  title: 'Oops...',
					  text: respuesta.mensaje
					})
				}

			});
		}
	</script>
</body>
</html>